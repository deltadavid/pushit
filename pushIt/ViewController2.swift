//
//  ViewController2.swift
//  pushIt
//
//  Created by David Lam on 26/11/14.
//  Copyright (c) 2014 David Lam. All rights reserved.
//


import UIKit

class ViewController2: UIViewController {
    
    var tmpTextBeforeViewDidLoad = ""
    
    @IBOutlet weak var someButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.setUpView()
    }
    
    func setUpView(){
        self.someButton.setTitle(tmpTextBeforeViewDidLoad, forState: UIControlState.Normal)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

