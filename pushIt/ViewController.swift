//
//  ViewController.swift
//  pushIt
//
//  Created by David Lam on 26/11/14.
//  Copyright (c) 2014 David Lam. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var someTextField: UITextField!
    @IBOutlet weak var specialButton1: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func setTextForSpecialButton1(sender: AnyObject) {
        var strForTitleOfSpecialButton1 = ""
        if self.someTextField.text == ""{
            strForTitleOfSpecialButton1 = "PlaceHolderText"
        }else{
            strForTitleOfSpecialButton1 = self.someTextField.text
        }
        self.specialButton1!.setTitle(strForTitleOfSpecialButton1, forState: UIControlState.Normal)
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "mySegue"{
            var destVC = segue.destinationViewController as ViewController2
            destVC.tmpTextBeforeViewDidLoad = self.specialButton1.titleLabel!.text!
        }
    }
}

